package hospital;

/**
 *
 * @author mitpatel
 */
public  class BasicWristBand {
    private String name;
    private String dob;
    private String doctor;
    
    public BasicWristBand(String name, String dob, String doctor) {
        this.name = name;
        this.dob = dob;
        this.doctor = doctor;
    }
   
    
    public String getname() {
        return name;
    }
    
    public String getdob() {
        return dob;
    }
    
    public String getdoctor() {
        return doctor;
    }
    
    
    public String toString()
    {
      
        return "Patient Name is : " +getname()+ "\nDate of birth is : " +getdob()+ "\nName of doctor is " +getdoctor();
    
    }
    
}
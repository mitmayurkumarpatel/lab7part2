
package hospital;

/**
 *
 * @author mitpatel
 */
public class HospitalSimulation {

    public static void main(String[] args) 
    {
        AllergyWristBand a1 = new AllergyWristBand("Charlie","20-11-1990","Dr. Paul","Fexofenadine");
        BasicWristBand b1 = new BasicWristBand("Mit", "01-11-2002", "Dr.x");
        ChildWristBand c1 = new ChildWristBand("Joey","11-2-2000","Dr. Paul","John");
        
        System.out.println(a1.toString());
        System.out.println();
        System.out.println(b1.toString());
        System.out.println();
        System.out.println(c1.toString());
        
    }
}

package hospital;

/**
 *
 * @author mitpatel
 */
public class ChildWristBand extends BasicWristBand{
    private String nameOfParent;
    
    //constructor 
    ChildWristBand(String name, String dob, String doctor, String nameOfParent) {
        super(name, dob, doctor);   //accessed with help of super keywordd from super class
        this.nameOfParent = nameOfParent;
    }
    
    //getter
    public String getnameOfParent() {
        return nameOfParent;
    }
    public String toString()
    {
        return "Patient Name is : " +getname()+ "\nDate of birth is : " +getdob()+ "\nName of doctor is " +getdoctor()+ "\nName of Parents is : " +nameOfParent;
    }
}
package hospital;

/**
 *
 * @author mitpatel
 */
public class AllergyWristBand extends BasicWristBand {
    private String allergy;
    
    //constructor 
    public AllergyWristBand(String name, String dob, String doctor, String allergy) 
    {
        super(name, dob, doctor);   //accessed with help of super keywordd from super class
        this.allergy = allergy;
    }
    
    public String getAllergy() {
        return allergy;
    }
    
    //to print attributes 
    public String toString()
    {
        return "Patient Name is : " +getname()+ "\nDate of birth is : " +getdob()+ "\nName of doctor is " +getdoctor()+ "\nName of Allergy is : " +this.allergy;
    }
}

